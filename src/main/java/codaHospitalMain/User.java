package codaHospitalMain;

import static constants.ApplicationConstants.*;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class User {
	private static final Logger logger = LoggerFactory.getLogger(Patient.class);
	private static final ResourceBundle message_Bundle = ResourceBundle.getBundle(message);
	public static void main(String args[])
	{
		Scanner input = new Scanner(System.in);
		UserLogin login=new UserLogin();
		logger.info(MessageFormat.format(message_Bundle.getString(HOS00028A), message_Bundle.getString(HOS0001A)));
int roleId=input.nextInt();
logger.info(MessageFormat.format(message_Bundle.getString(HOS00030A), message_Bundle.getString(HOS0001A)));
String userName=input.next();
String userpassWord=input.next();
int authentication=login.authorized(roleId,userName,userpassWord);
Patient patient=new Patient();
DoctorDAO doctor=new DoctorDAO();
switch(roleId)
{
case 1:
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00035A), message_Bundle.getString(HOS0001A)));
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00036A), message_Bundle.getString(HOS0001A)));
	int userChoice=input.nextInt();
	if(userChoice==1)
	patient.patientFunction(roleId,userName);
	else
	doctor.readDoctor();
	break;
case 2:
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00035A), message_Bundle.getString(HOS0001A)));
	patient.patientFunction(roleId,userName);
	break;
case 3:
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00037A), message_Bundle.getString(HOS0001A)));
	doctor.readDoctor();
	break;
}
	}

}
