package codaHospitalMain;

import static constants.ApplicationConstants.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dao.PatientDAO;
public class Patient {
	
	private static final Logger logger = LoggerFactory.getLogger(Patient.class);
	private static final ResourceBundle message_Bundle = ResourceBundle.getBundle(message);
	//PatientOperations patientOperations;

	public  void patientFunction(int roleId,String name) {
		Scanner input = new Scanner(System.in);
		PatientDAO operation = new PatientDAO();
		logger.info(MessageFormat.format(message_Bundle.getString(HOS00029A), message_Bundle.getString(HOS0001A)));
		String passWord=input.next();
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "vishali1");
			 PreparedStatement prep=con.prepareStatement("SELECT user_password FROM t_user_details WHERE user_name =?");
			 prep.setString(1,name);
			 ResultSet result=prep.executeQuery();
			 result.next();
			 System.out.println(result.getString("user_password"));
			 String str=result.getString("user_password");
		while (str.equals(passWord)) {
			logger.info(MessageFormat.format(message_Bundle.getString(HOS0001A), message_Bundle.getString(HOS0001A)));
			logger.info(MessageFormat.format(message_Bundle.getString(HOS0003A), message_Bundle.getString(HOS0001A)));
			logger.info(MessageFormat.format(message_Bundle.getString(HOS0004A), message_Bundle.getString(HOS0001A)));
			logger.info(MessageFormat.format(message_Bundle.getString(HOS0002A), message_Bundle.getString(HOS0001A)));

			String choice = input.next();
			switch (Operation.valueOf(choice)) {
			
			case INSERT:
				//operation.insertPatient();
				break;
			case UPDATE:
				operation.updatePatient();
				break;
			case DELETE:
				operation.deletePatient();
				break;
			case READ:
				operation.readPatient();
			default:
				logger.info(
						MessageFormat.format(message_Bundle.getString(HOS0007A), message_Bundle.getString(HOS0001A)));

			}
			con.close();
		}
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		}	
	}
