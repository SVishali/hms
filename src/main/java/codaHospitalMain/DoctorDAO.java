package codaHospitalMain;

import static constants.ApplicationConstants.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DoctorDAO {
	private static final Logger logger = LoggerFactory.getLogger(Patient.class);
	private static final ResourceBundle message_Bundle = ResourceBundle.getBundle(message);

	public  void readDoctor()
	{
		Scanner input = new Scanner(System.in);
		logger.info(MessageFormat.format(message_Bundle.getString(HOS00033A), message_Bundle.getString(HOS0001A)));
int doctorId=input.nextInt();
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "vishali1");
			String query="SELECT * FROM t_doctors";
			PreparedStatement prep=con.prepareStatement(query);
				ResultSet rs=prep.executeQuery(query);
				while (rs.next())
			      {
			        int id = rs.getInt("pk_doctor_id");
			        String doctorName=rs.getString("doctor_name");
			        String Qualification=rs.getString("doctor_Qualification");
			        System.out.format("%s, %s ,%s \n",id,doctorName,Qualification);
			      }
				
		con.close();	
		
	}
		catch(Exception e)
		{
			System.out.println(e);
	}
	}
}
