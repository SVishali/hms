package exceptions;

public class InvalidBufferException extends Exception{

	public InvalidBufferException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidBufferException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InvalidBufferException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidBufferException(String message) {
		super("File can't be read using bufferReader");
		// TODO Auto-generated constructor stub
	}

	public InvalidBufferException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
