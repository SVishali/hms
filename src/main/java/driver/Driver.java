package driver;

import static constants.ApplicationConstants.*;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dao.*;
import models.Patient;
import services.*;

public class Driver {
	private static final Logger logger = LoggerFactory.getLogger(Driver.class);
	private static final ResourceBundle message_Bundle = ResourceBundle.getBundle(message);

	public static void main(String args[]) throws SQLException {
		Scanner input = new Scanner(System.in);
		DoctorDAO doc = new DoctorDAO();
		Patient patientdetails=new Patient();
		AuthenticationService authentication = new AuthenticationService();
		logger.info(MessageFormat.format(message_Bundle.getString(HOS00032A), message_Bundle.getString(HOS0001A)));
		int userId = input.nextInt();
		logger.info(MessageFormat.format(message_Bundle.getString(HOS00030A), message_Bundle.getString(HOS0001A)));
		String userName = input.next();
		logger.info(MessageFormat.format(message_Bundle.getString(HOS00029A), message_Bundle.getString(HOS0001A)));
		String password = input.next();
		patientdetails=authentication.getUser(userId);
		if(patientdetails.getUserPassword().equals(password))
		System.out.println("Valid User");
		DoctorServices doctorService = new DoctorServices();
		PatientServices patientService = new PatientServices();
		ResultSet result = null;
		switch (userId) {
		case 1:
			logger.info(MessageFormat.format(message_Bundle.getString(HOS00035A), message_Bundle.getString(HOS0001A)));
			logger.info(MessageFormat.format(message_Bundle.getString(HOS00036A), message_Bundle.getString(HOS0001A)));
			int userChoice = input.nextInt();
			if (userChoice == 1)
				doctorService.getpatientFunction(userId, userName);
			else
				patientService.patientServices();
			break;
		case 2:
			logger.info(MessageFormat.format(message_Bundle.getString(HOS00035A), message_Bundle.getString(HOS0001A)));
			doctorService.getpatientFunction(userId, userName);
			break;
		case 3:
			logger.info(MessageFormat.format(message_Bundle.getString(HOS00037A), message_Bundle.getString(HOS0001A)));
			result = doc.readDoctor();
			while (result.next()) {
				int id = result.getInt("pk_doctor_id");
				String doctorName = result.getString("doctor_name");
				String Qualification = result.getString("doctor_Qualification");
				System.out.format("%s, %s ,%s \n", id, doctorName, Qualification);
			}
			result.close();
			break;
		}
	}
}
