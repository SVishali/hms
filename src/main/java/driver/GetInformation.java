package driver;
import static constants.ApplicationConstants.*;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import models.*;


public class GetInformation {
	private static final Logger logger = LoggerFactory.getLogger(Driver.class);
	private static final ResourceBundle message_Bundle = ResourceBundle.getBundle(message);
	Scanner patientDetails=new Scanner(System.in);
public Patient getPatientInfo()
{
	Patient patient=new Patient();
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00032A), message_Bundle.getString(HOS0001A)));
	int id=patientDetails.nextInt();
	patient.setUserId(id);
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00028A), message_Bundle.getString(HOS0001A)));
	int roleId=patientDetails.nextInt();
	patient.setUserRoleId(roleId);
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00031A), message_Bundle.getString(HOS0001A)));
	String name=patientDetails.next();
	patient.setUserName(name);
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00029A), message_Bundle.getString(HOS0001A)));
	String password=patientDetails.next();
	patient.setUserPassword(password);
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00042A), message_Bundle.getString(HOS0001A)));
	int bmi=patientDetails.nextInt();
	patient.setPatientBmi(bmi);
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00043A), message_Bundle.getString(HOS0001A)));
	int age=patientDetails.nextInt();
	patient.setUserAge(age);
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00045A), message_Bundle.getString(HOS0001A)));
	String mobileNumber=patientDetails.next();
	patient.setMobileNumber(mobileNumber);
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00044A), message_Bundle.getString(HOS0001A)));
	int branchNumber=patientDetails.nextInt();
	patient.setUserBranchNumber(branchNumber);
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00057A), message_Bundle.getString(HOS0001A)));
	String mailId=patientDetails.next();
	patient.setUserMailId(mailId);
	return patient;
}
public Doctor getDoctorInfo()
{
	Doctor doctor=new Doctor();
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00032A), message_Bundle.getString(HOS0001A)));
	int id=patientDetails.nextInt();
	doctor.setUserId(id);
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00028A), message_Bundle.getString(HOS0001A)));
	int role_id=patientDetails.nextInt();
	doctor.setUserRoleId(role_id);
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00028A), message_Bundle.getString(HOS0001A)));
	String doctor_name=patientDetails.next();
	doctor.setUserName(doctor_name);
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00044A), message_Bundle.getString(HOS0001A)));
	int branch_number=patientDetails.nextInt();
	doctor.setDoctorBranchId(branch_number);
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00045A), message_Bundle.getString(HOS0001A)));
	String mobileNumber=patientDetails.next();
	doctor.setMobileNumber(mobileNumber);
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00048A), message_Bundle.getString(HOS0001A)));
	int age=patientDetails.nextInt();
	doctor.setUserAge(age);
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00047A), message_Bundle.getString(HOS0001A)));
	String qualification=patientDetails.next();
	doctor.setDoctor_qualification(qualification);
	logger.info(MessageFormat.format(message_Bundle.getString(HOS00057A), message_Bundle.getString(HOS0001A)));
	String mailId=patientDetails.next();
	doctor.setUserMailId(mailId);

	return doctor;
	
}
}
