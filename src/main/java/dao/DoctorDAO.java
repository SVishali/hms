package dao;

import static constants.ApplicationConstants.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DoctorDAO  {
	private static final Logger logger = LoggerFactory.getLogger(DoctorDAO.class);
	private static final ResourceBundle message_Bundle = ResourceBundle.getBundle(message);

	public  ResultSet readDoctor()
	{
		logger.info(MessageFormat.format(message_Bundle.getString(HOS00033A), message_Bundle.getString(HOS0001A)));
		ResultSet rs=null;
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "vishali1");
			String query="SELECT * FROM t_doctors";
			PreparedStatement prep=con.prepareStatement(query);
				 rs=prep.executeQuery(query);
				
		con.close();	
		
	}
		catch(Exception e)
		{
			System.out.println(e);
	}
		return rs;
		
	}
	public void updateDoctor()
	{Scanner input = new Scanner(System.in);
		try
		{
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "vishali1");
logger.info(MessageFormat.format(message_Bundle.getString(HOS00016A), message_Bundle.getString(HOS0001A)));
int id = input.nextInt();
if(id==0)
{
	 logger.info(MessageFormat.format(message_Bundle.getString(HOS00017A), message_Bundle.getString(HOS0001A)));
}
else
{
		logger.info(MessageFormat.format(message_Bundle.getString(HOS00022A), message_Bundle.getString(HOS0001A)));
		logger.info(MessageFormat.format(message_Bundle.getString(HOS00046A), message_Bundle.getString(HOS0001A)));
		logger.info(MessageFormat.format(message_Bundle.getString(HOS00050A), message_Bundle.getString(HOS0001A)));
		int choice=input.nextInt();
		
		switch (choice) {
		case 1:
			logger.info(MessageFormat.format(message_Bundle.getString(HOS00051A), message_Bundle.getString(HOS0001A)));
			String newName=input.next();
			 PreparedStatement prep=con.prepareStatement("UPDATE t_doctor SET doctor_name=? WHERE doctor_id=?");
			prep.setString(1,newName);
			prep.setInt(2,id);
			prep.executeUpdate();
				logger.info(MessageFormat.format(message_Bundle.getString(HOS00053A), message_Bundle.getString(HOS0001A)));
			break;
		case 2:
			logger.info(MessageFormat.format(message_Bundle.getString(HOS00052A), message_Bundle.getString(HOS0001A)));
			String newQualification=input.next();
			 PreparedStatement prepared=con.prepareStatement("UPDATE t_doctors SET doctor_Qualification=? WHERE doctor_id=?");
			prepared.setString(1,newQualification);
			prepared.setInt(2,id);
			 prepared.executeUpdate();
			 logger.info(MessageFormat.format(message_Bundle.getString(HOS00054A), message_Bundle.getString(HOS0001A)));
			break;
		default:
			throw new IllegalArgumentException("Unexpected value: " + choice);
		}
		 con.close();	
}
}
	catch(Exception e)
	{
	System.out.println(e);	
	}

		}
	}

