package dao;

import java.sql.SQLException;

import codaHospitalMain.Patient;
	public interface DAOinterface {
		public int insertPatient(Patient patient) throws SQLException;

		public void readPatient();

		public boolean updatePatient();

		public boolean deletePatient();

	}
