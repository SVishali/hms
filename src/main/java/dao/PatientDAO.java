package dao;

import static constants.ApplicationConstants.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.MessageFormat;

import java.util.ResourceBundle;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import driver.*;
import models.Patient;

public class PatientDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(Patient.class);
	private static final ResourceBundle message_Bundle = ResourceBundle.getBundle(message);
	DbConnection connect=new DbConnection();
	Connection connection =null;
	 public void insertPatient(Patient patient)
	 { 
		try {
			
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "vishali1");
			 PreparedStatement prep=con.prepareStatement("INSERT INTO t_user_details(pk_user_id,user_name,user_age,user_mobile_number,fk_role_id_user,user_password,patient_bmi,patient_branch_id,patient_disease,user_mail_id) values(?,?,?,?,?,?,?,?,?,?)");
			prep.setInt(1,patient.getUserId());	
		prep.setString(2,patient.getUserName());
		prep.setInt(3,patient.getUserAge());
		prep.setString(4,patient.getMobile_number());
		prep.setInt(5,patient.getUserBranchNumber());
		prep.setString(6, patient.getUserPassword());
		prep.setInt(7,patient.getUserBranchNumber());
		prep.setInt(8,patient.getPatientBmi());
		prep.setString(9,patient.getPatientDisease());
		prep.setString(10,patient.getUserMailId());
	 prep.executeUpdate();
		logger.info(MessageFormat.format(message_Bundle.getString(HOS00027A), message_Bundle.getString(HOS0001A)));
		 con.close();	
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

}
	public void updatePatient() {

		Scanner input = new Scanner(System.in);
		
try {
	Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "vishali1");
	
		logger.info(MessageFormat.format(message_Bundle.getString(HOS00016A), message_Bundle.getString(HOS0001A)));
	 int id = input.nextInt();
	 if(id==0)
	 {
		 logger.info(MessageFormat.format(message_Bundle.getString(HOS00017A), message_Bundle.getString(HOS0001A)));
	 }
	 else
	 {
			logger.info(MessageFormat.format(message_Bundle.getString(HOS00022A), message_Bundle.getString(HOS0001A)));
			logger.info(MessageFormat.format(message_Bundle.getString(HOS00023A), message_Bundle.getString(HOS0001A)));
			logger.info(MessageFormat.format(message_Bundle.getString(HOS00024A), message_Bundle.getString(HOS0001A)));
			int choice=input.nextInt();
			
			switch (choice) {
			case 1:
				logger.info(MessageFormat.format(message_Bundle.getString(HOS00018A), message_Bundle.getString(HOS0001A)));
				String newName=input.next();
				 PreparedStatement prep=con.prepareStatement("UPDATE t_user_details SET user_name=? WHERE pk_user_id=?");
				prep.setString(1,newName);
				prep.setInt(2,id);
				prep.executeUpdate();
					logger.info(MessageFormat.format(message_Bundle.getString(HOS00026A), message_Bundle.getString(HOS0001A)));
				break;
			case 2:
				logger.info(MessageFormat.format(message_Bundle.getString(HOS00019A), message_Bundle.getString(HOS0001A)));
				int newAge=input.nextInt();
				 PreparedStatement prepared=con.prepareStatement("UPDATE t_user_details SET user_age=? WHERE pk_user_id=?");
				prepared.setInt(1,newAge);
				prepared.setInt(2,id);
				 prepared.executeUpdate();
				 logger.info(MessageFormat.format(message_Bundle.getString(HOS00025A), message_Bundle.getString(HOS0001A)));
				break;
			default:
				throw new IllegalArgumentException("Unexpected value: " + choice);
			}
			 con.close();	
	 }
}
		catch(Exception e)
		{
		System.out.println(e);	
		}
	}
	public void deletePatient() {
		Scanner input = new Scanner(System.in);
		
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "vishali1");
			 PreparedStatement prep=con.prepareStatement("DELETE FROM t_user_details where pk_user_id=?");
		logger.info(MessageFormat.format(message_Bundle.getString(HOS00020A), message_Bundle.getString(HOS0001A)));
		int id = input.nextInt();
		if (id==0) {
			logger.info(MessageFormat.format(message_Bundle.getString(HOS00017A), message_Bundle.getString(HOS0001A)));
		} else {
			prep.setInt(1,id);
			 prep.executeUpdate();
				logger.info(MessageFormat.format(message_Bundle.getString(HOS00021A), message_Bundle.getString(HOS0001A)));
		}
		 con.close();	
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		}
	public Patient readPatient(int userId)
	{
		Scanner input = new Scanner(System.in);
		Patient patient=new Patient();
		ResultSet rs=null;
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "vishali1");
			
			 String query="SELECT * FROM t_user_details WHERE pk_user_id=?";			
				PreparedStatement prep=con.prepareStatement(query);	
				prep.setInt(1,userId);
				 rs=prep.executeQuery();
				while (rs.next())
			      {
			        int id = rs.getInt("pk_user_id");
			        patient.setUserId(id);
			        String UserName = rs.getString("user_name");
			        patient.setUserName(UserName);
			       String MobileNumber=rs.getString("user_mobile_number");
			       patient.setMobileNumber(MobileNumber);
			       int age=rs.getInt("user_age");
			       patient.setUserAge(age);
			       String password=rs.getString("user_password");
			       patient.setUserPassword(password);
			       String mailId=rs.getString("user_mail_id");
			       patient.setUserMailId(mailId);
			      int branchId=rs.getInt("branch_id");
			      patient.setUserBranchNumber(branchId);
			        System.out.format("%s, %s, %s, %s, %s, %s ,%s\n",id,UserName,MobileNumber,age,password,mailId,branchId);
			 con.close();	
			      }	     
		
	}
		catch(Exception e)
		{
			System.out.println(e);
	}
		return patient;
}
	}
