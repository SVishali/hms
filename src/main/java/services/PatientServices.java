package services;

import static constants.ApplicationConstants.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import constants.PatientService;
import dao.DoctorDAO;
import driver.Driver;

public class PatientServices {
	Scanner input = new Scanner(System.in);
	private static final Logger logger = LoggerFactory.getLogger(Driver.class);
	private static final ResourceBundle message_Bundle = ResourceBundle.getBundle(message);

	public void patientServices() throws SQLException{
		try {
			logger.info(MessageFormat.format(message_Bundle.getString(HOS00022A), message_Bundle.getString(HOS0001A)));
			String patientChoice = input.next();
			DoctorDAO doctor = new DoctorDAO();
			switch (PatientService.valueOf(patientChoice)) {
			case READDOCTOR:
				logger.info(MessageFormat.format(message_Bundle.getString(HOS00055A), message_Bundle.getString(HOS0001A)));
				ResultSet result = doctor.readDoctor();
				break;
			case UPDATEDOCTOR:
				logger.info(MessageFormat.format(message_Bundle.getString(HOS00056A), message_Bundle.getString(HOS0001A)));
				doctor.updateDoctor();
				break;
			}
		} catch (Exception e) {
System.out.println(e);
		}
	}

}
