package models;



public  class Patient extends Users {
	private int patientBmi;
	private String patientDisease;
	public int getPatientBmi() {
		return patientBmi;
	}
	public void setPatientBmi(int patientBmi) {
		this.patientBmi = patientBmi;
	}
	public String getPatientDisease() {
		return patientDisease;
	}
	public void setPatientDisease(String patientDisease) {
		this.patientDisease = patientDisease;
	}
}