package models;

public class Doctor extends Users{
	private String doctorQualification;
	private int doctorBranchId;
	public String getDoctorQualification() {
		return doctorQualification;
	}
	public void setDoctor_qualification(String doctorQualification) {
		this.doctorQualification = doctorQualification;
	}
	public int getDoctorBranchId() {
		return doctorBranchId;
	}
	public void setDoctorBranchId(int doctorBranchId) {
		this.doctorBranchId = doctorBranchId;
	}

}
