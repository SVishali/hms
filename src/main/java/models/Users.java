package models;

public class Users {//User
	
	private int userId;
private String userName;
private String mobileNumber;
private int userAge;
private String userPassword;
private int userBranchNumber;
private int userRoleId;
private String userMailId;
public int getUserId() {
	return userId;
}
public void setUserId(int userId) {
	this.userId = userId;
}
public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}
public String getMobile_number() {
	return mobileNumber;
}
public void setMobileNumber(String mobileNumber) {
	this.mobileNumber = mobileNumber;
}
public int getUserAge() {
	return userAge;
}
public void setUserAge(int userAge) {
	this.userAge = userAge;
}
public String getUserPassword() {
	return userPassword;
}
public void setUserPassword(String userPassword) {
	this.userPassword = userPassword;
}
public int getUserBranchNumber() {
	return userBranchNumber;
}
public void setUserBranchNumber(int userBranchNumber) {
	this.userBranchNumber = userBranchNumber;
}
public int getUserRoleId() {
	return userRoleId;
}
public void setUserRoleId(int userRoleId) {
	this.userRoleId = userRoleId;
}
public String getUserMailId() {
	return userMailId;
}
public void setUserMailId(String userMailId) {
	this.userMailId = userMailId;
}

}
