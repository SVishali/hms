package codaHospital;

import static org.testng.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.testng.annotations.Test;

import models.Patient;

public class UserValidationTest {
	Patient patient=new Patient();
	@Test
	public void createPatientTestValid()  {
		
		try {			
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "vishali1");
		 PreparedStatement prep=con.prepareStatement("SELECT user_password FROM t_user_details WHERE user_mail_id =?");
		 prep.setString(1,"ani123@gmail.com");
		 ResultSet result=prep.executeQuery();
		 result.next();
		 String str=result.getString("user_password");
		 assertEquals("vishali",str);
		 con.close();	
		}
		catch(Exception e)
		{
			 
			System.out.println(e);
		}
		
		
	}
}
